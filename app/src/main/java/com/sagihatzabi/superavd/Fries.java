package com.sagihatzabi.superavd;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;

/**
 * Created by sagihatzabi on 05/02/2017.
 */

public class Fries extends SagiVectorIcon {

//    private @StyleableRes int[] styleableAttrs = R.styleable.BurgerView;
    final @DrawableRes int BASE_DRAWABLE_WITHOUT_ANIMATION = R.drawable.avd_fries_vector_anim;

    final int ANIMATION_DURATION = 501;
    public Type mType;

    final Handler animHandler = new Handler();

    public enum Type {
        Fries1("Fries", 1.99f, R.style.BurgerColorStyle),
        Fries2("Fries2", 1.99f, R.style.ChickenBurger),
        Fries3("Fries3", 1.99f, R.style.NoVeggBurger);

        private String stringValue;
        private float priceValueInDollars;
        private int styleValue;

        Type(String name, float price, @StyleRes int style) {
            stringValue = name;
            priceValueInDollars = price;
            styleValue = style;
        }

        String getName() {
            return stringValue;
        }

        float getPriceInDollars() {
            return priceValueInDollars;
        }

        int getStyle() {
            return styleValue;
        }

    }

    public Fries(Context context, Type friesType) {
        super(context, R.drawable.avd_fries_vector_anim, friesType.getStyle(), -1, -1);
        mType = friesType;
        mName = friesType.getName();
        mPrice = "" + friesType.getPriceInDollars();

        init(null, friesType, 0);
    }

    public Fries(Context context, Type friesType, int width, int height) {
        super(context, R.drawable.avd_fries_vector_anim, friesType.getStyle(), width, height);
        mType = friesType;
        mName = friesType.getName();
        mPrice = "" + friesType.getPriceInDollars();

        init(null, friesType, 0);
    }

    public Fries(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, null, 0);
    }

    public Fries(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, null, defStyleAttr);
    }

    public Fries(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs, null, defStyleAttr);
    }

    private void init(AttributeSet attrs, Type burgerType, int defStyleRes) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.BurgerView, defStyleRes, 0);

        this.mBaseDrawableWitoutAnimId = BASE_DRAWABLE_WITHOUT_ANIMATION;

        a.recycle();
    }

    private void updateDrawable(AnimatedVectorDrawable drawable) {
        this.setImageDrawable(drawable);
    }

    public void startAnimation() {
        ((AnimatedVectorDrawable)this.getDrawable()).start();
    }
}
