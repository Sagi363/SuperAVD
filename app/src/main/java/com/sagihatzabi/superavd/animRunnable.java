package com.sagihatzabi.superavd;

/**
 * Created by sagihatzabi on 11/02/2017.
 */

public class animRunnable implements Runnable {
    private Burger burger;
    private Burger.Type burgerType;

    public animRunnable(Burger burger) {
        this.burger = burger;
        this.burgerType = burger.mType;
    }

    @Override
    public void run() {
        switch (this.burgerType) {
            case NoVeggBurger:
                this.burger.removeVegg();
                break;
            case NoCheeseBurger:
                this.burger.removeCheese();
                break;
            case NoVeggAndCheeseBurger:
                this.burger.removeCheeseAndVegg();
                break;
            case NoSesameBurger:
                this.burger.bSesame = false;
                break;
        }
    }
}