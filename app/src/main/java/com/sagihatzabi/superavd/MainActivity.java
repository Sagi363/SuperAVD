package com.sagihatzabi.superavd;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Burger currBurger;
    TextView burgerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Add menu programmatically
        final LinearLayout menu = new LinearLayout(this);
        menu.setOrientation(LinearLayout.VERTICAL);

        for (int i=1; i<=5; i++) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.addView(createSnappingRecyclerView());
                }
            }, i * 500);
        }

        NestedScrollView nestedScrollView = new NestedScrollView(this);
        nestedScrollView.addView(menu);
        ((LinearLayout) findViewById(R.id.activity_main)).addView(nestedScrollView);
//
//        LinearLayout line1 = (LinearLayout) findViewById(R.id.line1);
//
//        Burger burger = new Burger(this, Burger.Type.BeefBurger);
//        line1.addView(burger);
//        burger.setOnClickListener(this);
//
//        burger = new Burger(this, Burger.Type.NoCheeseBurger);
//        line1.addView(burger);
//        burger.setOnClickListener(this);
//
//        burger = new Burger(this, Burger.Type.NoVeggBurger);
//        line1.addView(burger);
//        burger.setOnClickListener(this);
    }

    SnappingRecyclerView createSnappingRecyclerView() {
        SnappingRecyclerView mRecyclerView = new SnappingRecyclerView(this);
        mRecyclerView.enableViewScaling(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<SagiVectorIcon> customViews = new ArrayList<>();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        int iconSize = width/3;

//        customViews.add(new Burger(this, Burger.Type.BeefBurger, iconSize, iconSize));
        customViews.add(Burger.create(this)
                              .setType(Burger.Type.BeefBurger)
                              .setSize(iconSize, iconSize)
                              .setName("Sagi Test1")
                              .setPriceInDollars(9.99f)
                              .removeCheese()
                              .build());

        customViews.add(new Fries(this, Fries.Type.Fries1, iconSize, iconSize));
        customViews.add(new Steak(this, Steak.Type.Steak3, iconSize, iconSize));
//        customViews.add(new Burger(this, Burger.Type.ChikenBurger, iconSize, iconSize));
//        customViews.add(new Fries(this, Fries.Type.Fries2, iconSize, iconSize));
//        customViews.add(new Burger(this, Burger.Type.NoCheeseBurger, iconSize, iconSize));
//        customViews.add(new Steak(this, Steak.Type.Steak1, iconSize, iconSize));
//        customViews.add(new Burger(this, Burger.Type.NoSesameBurger, iconSize, iconSize));

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // specify an adapter (see also next example)
        MyAdapter mAdapter = new MyAdapter(customViews);
        mRecyclerView.setAdapter(mAdapter);

        return mRecyclerView;
    }

    @Override
    public void onClick(View view) {
        final Burger bur = ((Burger)view);

//        if (bur.mType == Burger.Type.NoVeggBurger) {
//            if (bur.bVegg) {
//                bur.removeVegg();
//            }else {
//                bur.addVegg();
//            }
//        }
//        else if (bur.mType == Burger.Type.NoCheeseBurger) {
//            if (bur.bCheese) {
//                bur.removeCheese();
//            }else {
//                bur.addCheese();
//            }
//        }
//        else if (bur.mType == Burger.Type.BeefBurger) {
//            if (bur.bVegg && bur.bCheese) {
//                AnimatedVectorDrawable d = bur.removeCheeseAndVegg();
//            }
//            else if ((!bur.bVegg && !bur.bCheese)){
//                bur.addCheeseAndVegg();
//            }


            ////// CHANGE THEME WITH ANIMATION //////
//            Drawable bbb = ResourcesCompat.getDrawable(getResources(), R.drawable.avd_burger_vector_anim, new ContextThemeWrapper(this, R.style.ChickenBurger).getTheme());
//            bur.setImageDrawable(bbb);
//            ((AnimatedVectorDrawable)bbb).start();

            ////// WTF?! - canApplyTheme always False ///////
//            ((AnimatedVectorDrawable)bur.getDrawable()).applyTheme(new ContextThemeWrapper(this, R.style.NoSesameBurger).getTheme());

            ////// NO WORK AT ALL ////////
//            Animator animator = (Animator) AnimatorInflater.loadAnimator(this, R.animator.animator_minus_vegg);
//            animator.setTarget(bur); // set the view you want to animate
//            animator.start();

    }
}
