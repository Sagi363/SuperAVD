package com.sagihatzabi.superavd;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.v4.content.res.ResourcesCompat;

import java.util.Queue;

/**
 * Created by sagihatzabi on 11/02/2017.
 */

public class animQueue {

    final private SagiVectorIcon icon;
    private Queue<Animatable2> animationDrawableQueue;

    public animQueue(SagiVectorIcon icon) {
        this.icon = icon;
    }

    public void addAnimation(@DrawableRes int drawableId) {
        final Resources.Theme currTheme = this.icon.mTheme;
        Animatable2 drawable = (Animatable2) ResourcesCompat.getDrawable(this.icon.getResources(), drawableId, currTheme);
        animationDrawableQueue.add(drawable);
    }

    private void runAnimations() {
//        for (Animatable2 animation : animationDrawableQueue) {
//
//        }
        for (int i=1; i<=animationDrawableQueue.size(); i++) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    icon.updateDrawable(animationDrawableQueue.poll());
                }
            }, i * 500);
        }
    }
}